# DriveJCS WP Serialized SQL DUMP Domain Transfer Utility #

WP Serial Migrate is an easy-to-use PHP utility that corrects string length values in serialized
data when you need to port a WordPress site from one domain name to another.  This PHP utility does NOT migrate
any website code.  It ONLY treats serialized data exported from a WordPress database so settings will be retained
when the domain name of the site changes for any reason.

This is a two-pass data transform utility.  The first pass searches for all occurrences of the old FQDN and replaces them
with the new FQDN and writes the results to a temporary working file.  The second pass uses that temporary working file
to inspect each line, determine whether that line contains serialized data with the FQDN and if it does, sets the length
attribute of the serialized value properly. Upon completion of the second pass, the utility writes a new SQL Dump file to
the ./data/output folder using <new-FQDN>.sql for the file name.  This new SQL dump file can then be imported directly
to the target database.

This is not an end-user plug-in for WordPress websites. It is intended for developers. Its use requires technical expertise, a working
PHP command-line environment, and a MySQL/MariaDB command line or GUI client capable of producing database export files.

### Current Version 1.0.0 ###

### Installation ###
Simply clone this repository into a project folder on your local machine.

### Scenario / Usage ###

* Fixing serialization length errors caused by moving a WordPress site at test.example.com to another WordPress site
at example.com

1. Export existing WordPress database to a single SQL file using DROP/CREATE options for the database and tables.
2. Copy the resulting SQL DUMP file to the utility's ./data/input/ folder.
3. Invoke the utility from the command line:
```php
php wp_migrate_to_new_domain.php ./data/input/<sql-dump>.sql old-fqdn new-fqdn
```
4. On completion, the utility will report the number of transforms performed on serialized data and the name of
the new SQL DUMP file (in ./data/output/).
5. Utilize MySQL command line or GUI client to import the database into the target database server.

### Elementor Limitations ###
This utility will not resolve serialized urls/domains created by the Elementor plug-in.  If you use this plug-in,
you can fix any remaining issues by using the Change URL function in the Elementor Tools WP Admin interface.

### Requirements ###

* PHP 7.4.x+
* MySQL DUMP file.

### Contact Me ###

* John Arnold <john@jdacsolutions.com>
* Website: https://jdacsolutions.com
