<?php
    /*
     * Project:    wp-serial-migrate
     * File:       wp_migrate_to_new_domain.php
     * Created:    Mar 14, 2022 13:34
     * Author:     John Arnold <john@jdacsolutions.com>
     * Author URI: https://drivejcs.com
     *
     * Description:  A utility for migrating WordPress databases containing serialized site URLs from old-domain to
     * new-domain.
     *
     * Copyright (c) 2022 John Arnold <john@jdacsolutions.com>
     * This is proprietary software.  All rights reserved. No warranty, explicit or implicit, is provided.
     * In no event shall the author be liable for any claim or damages.
     *
     * To change this template file, choose Settings | Editor | File and Code Templates
     */
    if (sizeof($argv) != 4) {
        die("\n  Invalid number of arguments received.  Expected 3 (source_file, source_url, target_url), received " . (sizeof($argv) -1) . "\n");
    }
    $root = "./";
    require( $root . 'vendor/autoload.php');

    $data = $argv[1];
    $source = $argv[2];
    $target = $argv[3];

    echo "\nData File: " . $data;
    echo "\nSource URL: " . $source;
    echo "\nTarget URL: " . $target;

    if (file_exists($data)) {
        $migrator = new \JCS_WP_Serial_Migrate\JCS_Serial_Transformer($source, $target, $data);
        echo "\nGlobally replacing $source with $target...";
        $temp_filename = $migrator->transform_domain();
        $inserts = $migrator->count_inserts($temp_filename);
        echo "\nNumber of VALUES groups to inspect: " . $inserts;
        $new_working = $migrator->transform_inserts($temp_filename);
        echo "\nSerialized Data Transforms completed: " . $migrator->serials_transformed;
        $new_sql_file = $migrator->finalize_transforms($temp_filename,$new_working);
        echo "\nNew SQL DUMP File: " . $new_sql_file;
    } else {
        die("Input file could not be found.");
    }
