<?php
    /*
     * Project:    wp-serial-migrate
     * Class:      JCS_Serial_Transformer
     * Created:    Mar 14, 2022 19:34
     * Author:     John Arnold <john@jdacsolutions.com>
     * Author URI: https://drivejcs.com
     *
     * Description:  The JCS_Serial_Transformer class is responsible for...
     *
     * Copyright (c) 2022 John Arnold <john@jdacsolutions.com>
     * This is proprietary software.  All rights reserved. No warranty, explicit or implicit, is provided.
     * In no event shall the author be liable for any claim or damages.
     *
     * To change this template file, choose Settings | Editor | File and Code Templates
     */


    namespace JCS_WP_Serial_Migrate;


    use JCS_WP_Serial_Migrate\utility\JCS_Helper;

    class JCS_Serial_Transformer {

        private string $source_domain;
        private string $target_domain;
        private string $input_file_name;
        public int $serials_transformed;

        public function __construct(string $old_domain, string $new_domain, string $input_file) {
            $this->source_domain = $old_domain;
            $this->target_domain = $new_domain;
            $this->input_file_name = $input_file;
            $this->serials_transformed = 0;
        }

        /**
         * Performs clean up and returns the filename and path of the new SQL Dump file.
         * @param string $first_temp filename and path of pass 1 temp file
         * @param string $final_temp filename and path of pass 2 temp file
         * @return string filename and path of transformed SQL Dump File
         */
        public function finalize_transforms(string $first_temp, string $final_temp): string {
            $new_sql = "./data/output/" . $this->target_domain . ".sql";
            if ( !copy($final_temp,$new_sql) ) {
                die("Could not create " . $new_sql);
            } else {
                unlink($first_temp);
                unlink($final_temp);
            }
            return $new_sql;
        }

        public function count_inserts(string $temp_file): int {
            $inserts = 0;
            if ($file = fopen($temp_file, "r")) {
                $helper = new JCS_Helper();
                while (!feof($file)) {
                    $next_line = fgets($file);
                    if ($helper->is_insert_clause($next_line)) {
                        $inserts++;
                    }
                }
                fclose($file);
            }
            return $inserts;
        }

        /**
         * Reads through each line in the working transform file, identifies VALUES clauses, and tests if they contain
         * serialized data with the new domain. If they do, adjusts the string length key for the string to match the
         * new length of the string before writing the line to the new working file.
         * @param string $temp_file working transform file name and path
         * @return string new working transform file name and path
         */
        public function transform_inserts(string $temp_file): string {
            $this->serials_transformed = 0;
            $helper = new JCS_Helper();
            $new_working = "./data/temp/" . $helper->random_filename();
            if ($current_working = fopen($temp_file, "r")) {
                $new_out = fopen($new_working, "w");
                while (!feof($current_working)) {
                    $next_line = fgets($current_working);
                    if (!$helper->is_insert_clause($next_line)) {
                        fputs($new_out,$next_line);
                    } else {
                        $transformed = $this->transform_serialized($next_line);
                        fputs($new_out, $transformed);
                    }
                }
                fclose($current_working);
                fclose($new_out);
            }
            return $new_working;
        }

        /**
         * @param string $values contains parenthesized, comma-separated list of values to insert
         * @return string new insert list with corrected string lengths on serialized string data or the original if
         * it does not contain the target domain as a serialized value
         */
        private function transform_serialized(string $values): string {

            $needle = ':"https://' . $this->target_domain;
            //$needle = ':"';
            if (!strpos($values,$needle, 0)) { return $values; }

            $elements = explode(",", $values);
            for ($i = 0; $i < sizeof($elements); $i++) {
                if ( strpos($elements[$i], $needle,0) ) {
                    // Contains serialized data with the new domain
                    $serials = explode(";",$elements[$i]);
                    for ($j = 0; $j < sizeof($serials); $j++) {
                        if (strpos($serials[$j],$needle,0)) {
                            $deepest = explode(":",$serials[$j]);
                            // We only care about strings like s:nn:"https://..."
                            if (sizeof($deepest) == 4 && $deepest[0] == 's') {
                                $temp_string = $deepest[2] . ':' . $deepest[3];
                                $deepest[1] = strlen($temp_string) - 2; // Don't count start and end "
                                $deepest_transformed = implode(":",$deepest);
                                $serials[$j] = $deepest_transformed;
                                $this->serials_transformed++;
                            }
                        }
                    }
                    $elements[$i] = implode(";",$serials);
                }
            }
            return implode(",", $elements);
        }

        private function is_serialized(string $candidate_string): bool {
            return (
                substr($candidate_string, 0,3) == "'a:" ||
                substr($candidate_string, 0,2) == "a:" ||
                substr($candidate_string, 0,2) == "s:"
            );
        }

        /**
         * Read through the input file, one line at a time and replace all occurrences of old-domain with
         * new-domain. Creates a temporary file and writes each modified line to the temporary file.
         * @return string The name of the temporary file so it can be used in subsequent passes of the transform.
         */
        public function transform_domain(): string {
            $lines = 0;
            $source_urls = 0;
            $helper = new JCS_Helper();
            $temp_file = "./data/temp/" . $helper->random_filename();
            if ($file = fopen($this->input_file_name, "r")) {
                $outfile = fopen($temp_file, "w");
                while(!feof($file)) {
                    $lines++;
                    $line = fgets($file);
                    // Handle typical https://...
                    $needle = "://" . $this->source_domain;
                    $needle_new = "://" . $this->target_domain;
                    $last_pos = 0;
                    while (($last_pos = strpos($line, $needle, $last_pos)) !== false) {
                        $source_urls++;
                        $last_pos = $last_pos + strlen($needle);
                    }
                    $new_line = str_replace($needle,$needle_new,$line);
                    // work with escaped https://...
                    /*$esc_needle = "https:\\\\/\\\\/" . $this->source_domain;
                    $esc_replace = "https:\\\\/\\\\/" . $this->target_domain;
                    $last_post = 0;
                    while (($last_pos = strpos($new_line, $esc_needle, $last_pos)) !== false) {
                        $source_urls++;
                        $last_pos = $last_pos + strlen($esc_needle);
                    }
                    $new_line = str_replace($esc_needle,$esc_replace,$new_line);
                    // Work with html_encoded
                    $html_needle = "https%3A%2F%2F" . $this->source_domain;
                    $html_replace = "https%3A%2F%2F" . $this->target_domain;
                    $last_post = 0;
                    while (($last_pos = strpos($new_line, $html_needle, $last_pos)) !== false) {
                        $source_urls++;
                        $last_pos = $last_pos + strlen($html_needle);
                    }
                    $new_line = str_replace($html_needle,$html_replace,$new_line);
                    */
                    fputs($outfile,$new_line);
                 }
                fclose($file);
                fclose($outfile);
            }
            echo "\nTotal Lines in File: " . $lines;
            echo "\nTemp File: " . $temp_file;
            echo "\nPerformed " . $source_urls . " replacements.";
            return $temp_file;
        }
    }
