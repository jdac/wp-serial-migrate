<?php
    /*
     * Project:    wp-serial-migrate
     * Class:      JCS_Helper
     * Created:    Mar 14, 2022 16:15
     * Author:     John Arnold <john@jdacsolutions.com>
     * Author URI: https://drivejcs.com
     *
     * Description:  The JCS_Helper class is responsible for...
     *
     * Copyright (c) 2022 John Arnold <john@jdacsolutions.com>
     * This is proprietary software.  All rights reserved. No warranty, explicit or implicit, is provided.
     * In no event shall the author be liable for any claim or damages.
     *
     * To change this template file, choose Settings | Editor | File and Code Templates
     */


    namespace JCS_WP_Serial_Migrate\utility;


    class JCS_Helper {

        public function random_filename(): string {

            $chars = implode(range("A","Z")) . implode(range("a","z")) . implode(range(0,9));
            $file_name_base = '';
            for ($i = 0; $i <= 14; $i++) {
                $index = rand(0, strlen($chars) - 1);
                $file_name_base .= $chars[$index];
            }
            return $file_name_base . ".sql";
        }

        public function is_insert_clause(string $line): bool {
            return (substr(trim($line), 0, 1) == '(');
        }

    }
